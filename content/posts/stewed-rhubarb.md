+++
date = "2022-05-07"
title = "Stewed Rhubarb"
tags = ['rhubarb', 'dessert']
+++

## What's this?

A recipe for when you have fresh rhubarb. I don't remember how much it makes. About a quart?

## Servings

About a quart?

## Ingredients

* About 700g of rhubarb (5 cups?)
* 1 cup granulated sugar
* About 2 TBSP water
* Zest of a small lemon

## Instructions

1. Mix everything together
1. Put everything in a saucepan/pot and simmer for 15-20 minutes, stirring pretty regularly, until it's the consistency you want
1. Bring to a quick boil at the end[^boil]
1. Let cool
1. Use[^use]


[^boil]: I don't know if this is actually needed, I just made it up because it seemed like it should boil at least for a little bit, like an old potion
[^use]: I used this as filling for little pastries and a pie
