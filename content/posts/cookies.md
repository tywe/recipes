+++
date = "2020-05-10"
title = "Chocolate chip cookies"
tags = ['cookies', 'dessert']
+++

## What's this?

The recipe for my favorite chocolate chip cookies. [Original source is here](https://smittenkitchen.com/2015/04/salted-chocolate-chunk-cookies/).

## Servings

About 16 cookies

## Ingredients

* 1/2 cup (1 stick) unsalted butter, **at room temperature** [^butter]
* 2 TBSP granulated sugar
* 2 TBSP Sugar in the Raw (turbinado sugar, or use brown sugar if not available)
* 3/4 cup + 2 TBSP packed brown sugar[^cups]


* 1 egg (large is better)
* 1 tsp vanilla extract


* 3/4 tsp baking soda
* Heaped 1/4 teaspoon salt (~3/8 tsp)


* 1 3/4 cups all-purpose flour
* 4-6 oz (about half a bag) **dark/bittersweet** chocolate chips (60% cacao is excellent) [^chips]

## Instructions [^mixer]

1. Preheat over to 360F
1. Prep a baking sheet, ideally with parchment paper or a silicone mat
1. Mix the butter and sugars (the first set of ingredients) on a high setting until fluffy
    * 7-8 minutes is usually good. Overdoing it slightly is better than underdoing it.
    * It should be kind of like a thick cream
    * If your butter is chilled, this will take longer
1. Mix in the egg and vanilla until just combined
1. Mix in the baking soda and salt until just combined
1. Slowly add and mix in the flour until just combined, keeping the dough a little crumbly
1. Fold in chocolate chips, keeping dough crumbly
1. Scoop/roll into balls on the baking sheet
1. Bake for 8-12 minutes, depending on desired level of doneness
    * Once they're out, they'll bake a little more
    * 9-10 minutes results in my preferred texture once cooled, but it depends on the oven and will be a little different every time

Enjoy!

[^butter]: This is important. There are ways to room-temperature-ize a stick quickly if needed, but just keeping one out for several hours is still the best.
[^cups]: Same as 1 cup minus 2 TBSP if that's easier to measure
[^chips]:  These go in last, so eyeball it depending on the ratio you prefer
[^mixer]: I do all the mixing in a stand mixer, but you could do this with a hand mixer or similar. While mixing, scrape down the sides periodically.
