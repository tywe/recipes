+++
date = "2025-01-15"
title = "Chicken Caesar Crunchwrap Supreme"
tags = ['chicken', 'main dish']
+++

## What's this?

A recipe for a Chicken Caesar Crunchwrap-Supreme-like thing. Source here [^cody].

Sizing estimates:

* Final size should be about 5 inches smaller than the overall tortilla size. So if you're using 10-inch tortillas, the final filling should take up the middle 5-6 inches or so.
* Unofficial numbers on the official CWSup estimate a 12-inch tortilla and a 7-inch diameter.

## Servings

Six I guess? He makes 6 tortillas. He also fries up 6 chicken thighs, so use about 1 thigh per wrap.

## Ingredients

### Tortillas

Either:

* Tortillas bought in store
    * Tortillas should be about 12 inches or so
* Home-made tortillas (quantities not specified, DIY)
    * Flour
    * Salt
    * Baking powder
    * Lard
    * Hot water

### Dressing

This is his recipe, but use what Caesar dressing you like, really. Whatever you like.

* 1 egg yolk
* 1-2 anchovy fillets
* 2 cloves garlic (crushed)
* A little worchestershire sauce
* About 2 tsp dijon/english mustard
* Half a lime of lime juice, couple tablespoons
* About 1/4 cup good olive oil
* Black pepper to taste
* Handful of parmesan cheese
* Pinch of crushed red pepper

### Parmesan crisp

* 1/4 cup parmesan cheese? Enough to make a thin crispy circle

### Chicken

* 6 chicken thighs (whatever though)
* Salt
* Pepper
* Paprika

### Our little salad

* Head of romaine
* Croutons

## Instructions

If making homemade tortillas, make them. If using store-bought tortillas, get them out. Steam them up once the filling is ready. Recommendation: Heat them in a large pan until warm and then wrap them in a towel until ready.

### Make the infixin's

#### Dressing

1. Whisk together egg yolk, anchovy fillets, crushed garlic, worchestershire sauce, mustard, and lime juice
1. Slowly whisk in olive oil to emulsify
1. Add other ingredients to taste

#### Parmesan crisp

1. Sprinkle into a nonstick or greased pan into a circle[^bake]. It should be about the size you want the finished round wrap to be in your hand (5-7 inches, depending on tortilla size)
1. Fry up until golden brown
1. Remove while still soft; it'll crisp up further after cooling


#### Chicken

1. Season chicken with salt, pepper, and paprika
1. Fry up in a pan with seasoning side down
    * Season other side while in pan
1. No instructions on time, this is just you cooking
1. Once cooked, remove from pan and chop into bite-sized pieces
1. Toss in a bowl with some of the dressing, maybe about 1/3 of it

#### Our little salad

1. Chop romaine into bite-sized pieces
1. Roughly chop croutons so they aren't huge
1. Toss in the rest of the dressing

### Build it

On the tortilla:

1. Add a layer of our little salad in a circle in the center. There should be ~2.5 inches of tortilla left around the outside.
1. Add the parmesan crisp as the next layer
1. Add the chicken as the next layer
1. Fold it up[^fold]

### Cook it

1. Toast one while you keep building the rest
1. Having something to press down while toasting seems like it would help

Enjoy!

[^cody]: [TikTok](https://www.tiktok.com/@codytriesstuff/video/7459876769823444270), [YouTube](https://www.youtube.com/shorts/vdra2TLf_xA)
[^bake]: Alternatively, bake in oven (Temp up to you! Not specified! Figure it out! It's fine!) on parchment/silpat
[^fold]: (from [reddit](https://old.reddit.com/r/food/comments/15ejqtr/comment/juabtly)): "Fold the tortilla in towards the center on one side, then fold one of the newly formed new corners in, repeat, it will be like 6 folds total in the end."
