+++
date = "2020-07-11"
title = "Lemon sour cream cookies"
tags = ['lemon', 'cookies', 'dessert']
+++

## What's this?

Modified recipe from who knows where. You might say these Lemon Sour Cream Cookies have a unique fluffy, moist, sturdy texture, slightly tangy and not overly sweet.

Takes about 1 1/2 hours, including refrigeration time.

## Servings

Makes about 25 cookies?

## Ingredients

* 1 1/2 cups all-purpose flour
* 1/2 teaspoon baking powder
* 1/4 teaspoon baking soda
* 1/8 teaspoon salt
* 8 tablespoons unsalted butter softened (1 stick)
* 3/4 cup granulated sugar
* 1 large egg
* 1/2 cup sour cream
* 2 teaspoons grated zest and 1/8-1/4 cup juice from 1-2 lemons, depending on how much lemon you like
* 1 tablespoon cream cheese, softened
* 1 1/2 cups powdered sugar


## Instructions

### 1. Prep

1. Adjust oven racks to upper-middle position and heat oven to 375 degrees
1. Line baking sheet with parchment paper

### 2. Dry ingredients

1. Whisk flour, baking powder, baking soda, and salt in large bowl

### 3. Wet ingredients

1. On medium-high speed, beat butter and sugar until light and fluffy, about 2 minutes
1. Add egg and mix until combined
1. Reduce speed to medium-low and mix in sour cream and zest
1. Add flour mixture from the Dry Ingredients step and mix until incorporated.

### Refrigerate

Refrigerate about an hour. Should be slightly firm.

### Bake

Drop tablespoons of dough onto baking sheet. Bake until just golden around edges, about 15 minutes, switching and rotating sheet halfway through baking. Cool on baking sheet. Repeat as needed with extra dough.


### Make the glaze while baking

1. Combine cream cheese and lemon juice in medium bowl
2. Whisk in powdered sugar until smooth
3. Top each cooled cookie with 1 teaspoon glaze

### Eat

Eat them
