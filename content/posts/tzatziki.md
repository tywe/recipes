+++
date = "2016-12-04"
title = "Pork Souvlaki with Tzatziki"
tags = ['greek', 'pork', 'main dish']
+++

## What's this?
A favorite recipe of mine.[^link]

[^link]: Archive of original is [here](https://web.archive.org/web/20120716021434/http://ceramiccanvas.com/2009/09/pork-souvlaki-with-tzatziki/).

## Servings

About 4.

## Time

Anywhere from 1 to 3 hours, depending on how long you're okay with letting the pork marinate.

## Ingredients

* 1 1/4 lbs pork loin
* 1 large onion
* 1/4 cup plus 2 Tbsp extra-virgin olive oil
* 3 lemons, plus any extras for garnish
* 2 tsp dried oregano
* 3 garlic cloves (or equivalent paste)
* Kosher salt and freshly ground pepper
* 1 cup Greek whole-milk yogurt
* 1/2 English/Seedless cucumber
* 2 Tbsp mint
* Warm pita, for serving

## Prepare pork

1. Cut the pork loin into half-inch strips, about 3 inches long.
2. Cut the onion into half-inch wedges.
3. Juice the lemons (and cut wedges for garnish if extra)
4. Mash the garlic cloves into a paste, and divide into 2/3 and 1/3.

In a medium bowl, combine the pork, onion, 2/3 of the garlic paste, lemon juice, olive oil, and oregano. Add 1 1/2 tsp of salt and 1/2 tsp pepper.

Cover and refrigerate for at least 30 minutes, or up to 2 hours.

## Make tzatziki

Seed and finely dice the cucumber and chop the mint.

In a bowl, mix the yogurt, cucumber, mint, and remaining garlic paste. Season to taste with salt and pepper.[^lemon]

[^lemon]: Lemon juice is also a tasty addition

Stick in fridge until ready to serve.

## Cook pork

Once pork is finished marinating, heat a large frying pan until it's nice and hot.[^pan]

Cook pork, onions, and marinade in pan over high heat, turning once or twice, until the pork has a good sear and onions are tender and caramelized.[^cook]

[^pan]: I like a stainless steel pan here, since it's easiest to get a good sear. A cast-iron would also work, but I've found that it retains the moisture a little more than a stainless pan does.
[^cook]: You'll probably need to do this in batches. You'll want a good browned exterior, until it kinda looks like gyro meat or kebab meat. With the marinade in the pan, it's easy to cook the pork without getting the sear, so watch your moisture levels.

## Finish

Transfer the pork and onion to plates and serve with the tzatziki, lemon wedges and pita.
