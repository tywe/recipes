+++
date = "2021-05-12"
title = "Coleslaw"
tags = ['coleslaw', 'cabbage', 'dressing']
+++

## What's this

Homemade coleslaw recipe. Prep is done the day before, but would work without it.

I like coleslaw and have recently been getting bolder about making my own. Below are my experiments based on multiple sources.


## Servings

**I'm using half a head of cabbage as a baseline** so 4-5 servings or so. Double the amounts if you're using a full head.

## Coleslaw No. 1

This is the section for my regular ol' coleslaw and will be updated/edited as needed while my tastes change and I perfect the recipe. If I do something drastically different, it'll be under a different recipe number.

### Ingredients

I  like creamy coleslaw but not sweet, which usually means the recipes are heavy on the mayo and vinegar and have little to no sugar. Adjust as you like.

* Half cabbage[^cabbage]
* 1/2 cup mayo[^mayo]
* 2 Tbsp rice vinegar[^vinegar]
* 2 Tbsp cider vinegar
* 1 1/2 tsp Penzey's Justice seasoning[^justice]
* Big ol' sprinkle of black pepper (1/2 tsp?)
* Small sprinkle of table salt

### Instructions

_These instructions are for overnight prep. If you're making this all in one go, just slice the cabbage, make the dressing, and combine._

1. Slice the cabbage and rinse and drain with cold tap water
1. Boil some water. I just started a kettle of 5-6 cups[^overnight]
1. Once water is boiling, pour over cabbage then drain well. [^overnight]
1. Once drained, put into a plastic bag with a paper towel and store in the fridge overnight or until ready to combine with the dressing[^overnight]
1. For the dressing, combine all vinegar(s), mayo, and seasoning, ideally in a jar or some other sealable container. Leave the salt and pepper for last. Adjust seasonings to taste and keep in the fridge until ready to combine.

When you're ready to serve the coleslaw (or an hour or so ahead of time), combine the coleslaw and the dressing. Season to taste.

Enjoy!

[^cabbage]: I've tried green cabbage and red cabbage. Both great, others should work too!
[^vinegar]: I find that rice vinegar adds a nice acidity and sweetness - if you don't have this, using white vinegar and a pinch of sugar should be ok. Taste as you go!
[^justice]: I substituted this from a [recipe that called for Penzey's Fox Point Seasoning](https://wisconsinhomemaker.com/shredded-oil-and-vinegar-coleslaw-recipe/) so either of those should be OK, or work out your own substitute. If you use Fox Point, you might not need as much salt. (Justice blend ingredients are shallots, garlic, onion, green peppercorns, chives, and green onion; Fox Point ingredients are salt, freeze-dried shallots, chives, garlic, onion and green peppercorns)
[^overnight]: Only do these steps if you're prepping the coleslaw ahead of time
[^mayo]: This can be heavily adjusted based on your creaminess preference. I've used 3/4 cup when preparing ahead of time just so I'd have extra if needed, but have used 1/2 cup when making it immediately.
