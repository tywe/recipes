+++
date = "2024-05-14"
title = "Boneless leg of lamb"
tags = ['lamb', 'entree', 'roast']
+++

## What's this?

Mix of a couple of recipes for a fancy yet set-and-forget roast

Sources:

* [Crowd Cow's recipe](https://www.crowdcow.com/recipe/a3cmecsse/herb-rubbed-roasted-boneless-leg-lamb)
* [Dizzy Pig's Mediterranean-ish grill recipe](https://dizzypigbbq.com/recipe/charcoal-roasted-boneless-leg-of-lamb/)


## Servings

How many servings are in a 4lb. leg of lamb? Who knows?

## Ingredients

* 1 4lb boneless leg of lamb
* 1 head garlic (5-6 cloves or whatever)
* 1/4 cup ghee[^ghee]
* 1/4 cup of whatever spice blend you fancy[^spices]
* Salt and pepper as needed


## Instructions

### 0. Prep

* Preheat the oven to 475
* Put the lamb in a baking dish. Keep the netting on.

### 1. Garlic and spices

_Time: 20 minutes_

1. Peel the cloves of garlic from the head
1. Melt the ghee in a small cast iron pan or other small saucepan
1. On low heat, put the garlic in and let simmer for about 15 minutes. You want to bring out the flavor with a light roast, no toasting or burning here
1. Once done, strain the cloves out
1. Smash the cloves into a paste and put them back into the oil
1. Add the rest of the seasoning to the oil

### 2. Lamb

_Time: 1h30m_

1. Rub the lamb all over with the seasoning/oil/garlic blend from before, along with some salt and pepper
1. Put the lamb in the oven for 20 minutes
1. After 20 minutes, turn the temp down to 350F
1. Roast for another 30 minutes, perhaps more, until the internal temp is about 140F[^temp]
1. Remove from oven and let rest 15 minutes

### Eat

Eat it. But let it rest first! Remember that.


[^ghee]: The grilling recipe suggested olive oil, which should be OK for 350F, but we're doing a short burst of 475. Could try avocado oil, but we had ghee so I'm going to try that
[^spices]: I used about a 2:1 ratio of Penzey's Lamb seasoning and Penzey's Sate seasoning because it's what was in the house.
[^temp]: 145F is medium-rare, and we'll let it rest, so aiming for medium/med-rare here
