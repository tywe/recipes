+++
date = "2018-04-07"
title = "Manti"
tags = ['turkish', 'beef', 'lamb', 'dough', 'main dish']
+++

## What's this?

A favorite recipe of mine. Turkish ravioli/dumplings.
Based on a combination of information from three sources. [^1] [^2] [^3]
[^1]: [Source 1](http://www.panningtheglobe.com/2013/11/05/turkish-manti/)
[^2]: [Source 2](http://www.tasteofbeirut.com/manti/)
[^3]: [Source 3](https://ozlemsturkishtable.com/2013/12/manti-the-tiny-treasuresturkish-dumplings-stuffed-with-ground-meat-in-garlic-yoghurt-and-spices/)

## Pronunciation

Kinda like "mahn-tuh", according to [this video](https://www.youtube.com/watch?v=Bybqy29e7Yw).

## Servings

Like, four. Maybe six.

## Time

A solid afternoon or evening.

A speed run could be 60-90 minutes. I'd budget 2-3 hours.  

Dough should rest for 30 minutes. The most time-consuming part by far could be the folding of the dumplings, but that totally depends on how small you make them.

## Full list of ingredients

* 1 onion, small
* 1 lemon
* 2 garlic cloves
* 4 Tbsp parsley (a small bunch)
* 1 egg
* 1/4 cup mint (or a small pack from the store)
* 1/2 pound ground beef, lamb, or a mix
* 1/2 cup olive oil[^oil]
* 2 cups flour
* 6oz tomato paste (1 small can)
* 3 Tbps unsalted Butter
* 2 cups Greek yogurt (about half of a tub)
* 1 1/2 tsp paprika[^paprika] [^aleppo]

* 1/2 tsp red pepper flakes
* 3 tsp sea salt or more, to taste

[^oil]: This will be split between different parts of the recipe
[^paprika]: Sweet/Hungarian if you've got it, but smokey/Spanish is still delicious
[^aleppo]: You could also use aleppo instead of the paprika/red pepper mix

## Ingredients by component

### Dough

* 2 cups flour
* 1 egg
* 1/4 cup water
* 2 Tbps olive oil
* 1 tsp sea salt

### Filling

* 1/2lb ground beef/lamb/mix
* 1 onion
* 1/4 cup chopped parsley
* 1 tsp sea salt

### Yogurt sauce

* 2 cups Greek yogurt
* 2 cloves garlic
* 1/2 tsp sea salt, or to taste

### Tomato sauce

* 6oz tomato paste
* 4 Tbsp olive oil
* 1 cup water
* 1 1/2 tsp paprika
* 1/2 tsp red pepper flakes

### Butter sauce

* 3 Tbps unsalted butter
* 3 Tbps olive oil
* Sea salt to taste

### Topping

* Lemon zest
* Mint


## Instructions

### Make the Dough

Pour flour and salt into a pile on a work surface or large board. Make a well in the center of the flour and crack the eggs into it.

Start with a fork, mixing the eggs and incorporating the flour. When the mixture is too thick for stirring with a fork, start using your hands to mix everything together, gradually adding the water and oil, as you start to knead and press the dough.

Knead dough for 8-10 minutes until you get a firm, smooth dough.

Cover with a damp cloth and let it rest for 30 minutes.

### Make filling

While the dough rests, combine lamb, onion, parsley and salt in a small bowl. Set aside.

### Preheat oven

To 325F.

### Make yogurt sauce

Mix all ingredients in a small bowl. Store in the fridge. [^temp]

[^temp]: Bring to room temperature before serving. But it's ok if it's chilled, too.

### Fill Manti

Line 2 baking sheets with parchment paper.

Cut dough into fourths[^dry].

Roll dough on floured surface, into a thin sheet, about 1/16 of an inch thick. Use a knife or a pizza cutter to cut into 1½-inch squares.[^ruler]

Add a small ball of meat - about the size of a chickpea - to the center of each square. Pull the four corners up around the meat and press the four side seams firmly together to seal. Set manti onto baking sheet.

Repeat with the rest of the dough. [^count] [^extra] Bake manti for 15 minutes, until it just starts to brown. Set aside once it's done. [^storage]

Allow to come to room temp before continuing.

[^dry]: Work with one piece at a time, leaving others covered to prevent drying
[^ruler]: Some kind of straightedge like a ruler is helpful here
[^count]: I've found this usually makes about 120 manti total
[^extra]: If you have extra filling, you can mold them into meatballs and bake them for 15-20 minutes at 400F after removing the manti
[^storage]: At this point you can let the manti cool and store it in the fridge for up to a day or in the freezer for up to two weeks.

### Tomato sauce

Cook oil and tomato paste in a small pot over medium low heat, stirring constantly, for about 7 minutes, until you get a deep rich color and oil is incorporated.

Add paprika and red pepper and cook, stirring for 30 seconds longer. Remove from heat.

Add water and whisk to combine[^bad]. Set aside. Reheat just before serving.

[^bad]: At first this might seem like it'll be a watery mess. Don't worry, keep whisking.

### Butter sauce

Melt butter in a small saucepan over medium heat. Whisk constantly for 1-3 minutes, until you see little brown flecks appear. Remove from heat. Add olive oil. Set aside.

### Boil

Bring a large pot of salted water to a boil. Add manti and cook at a gentle boil 10-12 minutes, until tender. Drain and return to pot.

Pour butter sauce over and toss to coat, reheating gently, if need be. Season with salt, to taste.

### Finally

Divide manti between bowls. Spoon on the tomato sauce and yogurt. Drizzle on any extra butter sauce. Sprinkle with spices.

Enjoy!

## Photos

{{< figure
  src="../img/manti1.jpg"
  class="smaller"
  caption="BD (Before Dough)"
>}}

{{< figure
  src="../img/manti2.jpg"
  class="smaller"
  caption="DB (Dough Ball)"
>}}

{{< figure
  src="../img/manti3.jpg"
  class="smaller"
  caption="Dough cut into squares"
>}}

{{< figure
  src="../img/manti4.jpg"
  class="smaller"
  caption="Mostly through filling the dough"
>}}

{{< figure
  src="../img/manti5.jpg"
  class="smaller"
  caption="Cooking"
>}}

{{< figure
  src="../img/manti6.jpg"
  class="smaller"
  caption="Ready!"
>}}
