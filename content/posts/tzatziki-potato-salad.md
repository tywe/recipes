+++
date = "2023-09-06"
title = "Instant Pot Tzatziki Potato Salad"
tags = ['tzatziki', 'potato-salad', 'instant-pot']
+++

## What's this?

Mix of a couple of recipes to make a quick potato salad with the power of the instant pot.

Sources:

* [Instant Pot Potato Salad - TBFS](https://tastesbetterfromscratch.com/instant-pot-potato-salad/)
* [Tzatziki Potato Salad - Smitten Kitchen](https://smittenkitchen.com/2012/05/tzatziki-potato-salad/)
* [Amy + Jacky](https://www.pressurecookrecipes.com/instant-pot-potato-salad/)

## Servings

4-6. The original recipe used 4lb of potatoes - I've cut that in half and kept the rest the same.

## Ingredients

* 2lb of potatoes, washed and cut into potato-salad-sized chunks (about 1 inch cubes)
* About 2 cups water
* 1 3/4 cups greek yogurt
* 1/4 cup sour cream
* Juice from half a lemon (about 2 tbsp)
* 1 tbsp rice vinegar (for potatoes) (white wine vinegar works too)
* 2 tbsp rice/white wine vinegar (for finishing)
* 2 tablespoons minced fresh dill
* 1 minced sprig of mint, optionally
* 1 medium garlic clove, minced
* 2 teaspoons kosher salt
* Freshly ground black or white pepper
* ~1 lb cucumber (baby cumcumbers or english work best), unpeeled, grated


## Instructions

### 0. Prep

* Chop the potatoes into ~1 inch cubes
* Mince the dill
* Mince the garlic
* Juice the lemon
* Grate the cucumber (if there's a lot of juice, give it a quick squeeze in a colander/cheesecloth/strainer)

### 1. Potatoes

_Time: 25-30 minutes_

1. Chop potatoes if you didn't see that part in the ingredients already
1. Put the potatoes in the instant pot with enough water to cover them - err on the side of more. 2 cups is what I've seen recommended.
1. (Bonus) Throw a couple eggs in there if you'd like some hard-boiled eggs! This recipe doesn't use them but wouldn't hurt to make some
1. Secure the instant pot lid and turn the valve to sealing
1. Select high pressure/manual and set timer to 0 minutes. It should take 20-25 minutes to get to pressure
1. When the timer beeps, quick release the pressure

If the potatoes are still too firm, put the lid back on, leave it alone, and check in every couple of minutes

Drain the pot. Let the potatoes cool.

### 2. Finish it

1. While the potatoes are cooking, cooling, or whatever depending on your timing, combine the:
    * Yogurt
    * Sour cream
    * Lemon juice
    * Dill
    * Garlic
    * Remaining vinegar
    * Cucumber
1. Add salt and pepper to taste

When you're ready and the potatoes are cool, add the potatoes to the yogurt mixture. Taste and add more seasoning as needed.

### Refrigerate

Refrigerate until cool and ready to serve.

### Eat

Eat it
