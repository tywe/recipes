# Recipes

Recipes posted here probably fit one or more of these criteria:

* I've cobbled them together from different sources
* I've customized them heavily with notes
* They no longer exist on their original page
* They're so commonly referenced in this household that I need an easy place to find them